1. Initialize a debian-based server with ssh and python >= 3.10
2. make configure
3. make deploy
4. certbot:
    apt install python3-certbot-nginx
    certbot --nginx
5. Copy media/, locale/ and pages/locale to server's /var/www/neblina/
6. make dump
7. Copy db.json to server's /var/www/nebilna
8. In server:
  cd /var/www/nebilna
  set -a; . .env; set +a
  . venv/bin/activate
  ./manage.py compilemessages
