from ckeditor import fields
from .widgets import CKEditorUploadingWidget

class RichTextUploadingFormField(fields.RichTextFormField):
    widget = CKEditorUploadingWidget

    def __init__(
        self,
        config_name="default",
        extra_plugins=None,
        external_plugin_resources=None,
        get_extra_styles=None,
        *args,
        **kwargs
    ):
        kwargs["widget"] = self.widget(
            config_name=config_name,
            extra_plugins=extra_plugins,
            external_plugin_resources=external_plugin_resources,
            get_extra_styles=get_extra_styles,
        )

        super(fields.RichTextFormField, self).__init__(*args, **kwargs)

class RichTextUploadingField(fields.RichTextField):
    def formfield(self, **kwargs):
        defaults = {
            "form_class": RichTextUploadingFormField,
            "get_extra_styles": self.get_extra_styles,
        }
        defaults.update(kwargs)
        return super().formfield(**defaults)

    def __init__(self, *args, get_extra_styles=None, **kwargs):
        self.get_extra_styles = get_extra_styles
        super().__init__(*args, **kwargs)


