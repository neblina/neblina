# from .models import Configuration
from ckeditor_uploader import widgets

class CKEditorUploadingWidget(widgets.CKEditorUploadingWidget):
    def __init__(self, get_extra_styles, *args, **kwargs):
        self.get_extra_styles = get_extra_styles
        super().__init__(*args, **kwargs)

    def get_context(self, *args, **kwargs):
        if self.config_name == "pages":
            if self.get_extra_styles:
                extra_styles = self.get_extra_styles()
            else:
                extra_styles = None

            if 'stylesSet' in self.config:
                if 'regular_styles' in self.config:
                    self.config['stylesSet'] = self.config['regular_styles'] + extra_styles
                else:
                    self.config['regular_styles'] = self.config['stylesSet'].copy()
                    self.config['stylesSet'] += extra_styles
            elif extra_styles:
                self.config['stylesSet'] = extra_styles

        return super().get_context(*args, **kwargs)

