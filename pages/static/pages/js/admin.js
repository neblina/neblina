if (typeof CKEDITOR != 'undefined' ) {
  CKEDITOR.on('instanceReady', function(evt) {
    if (window.location.pathname.includes('pages/page/add')) {
      var el = document.getElementsByClassName('cke_button__image')[0];
      if (el) {
        el = el.closest('.cke_toolbar');
        el.style.display = 'none';
      }
    }

  });
  CKEDITOR.on('instanceCreated', function(e) {
    custom_css = document.getElementById('id_custom_css');
    if (custom_css)
      CKEDITOR.addCss(custom_css.innerHTML);
  });
}
