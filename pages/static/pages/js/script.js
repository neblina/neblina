// TODO: don't use global scope

function slugify(text, separator) {
  text = text.toString().toLowerCase().trim();

  const sets = [
    { to: "a", from: "[ÀÁÂÃÄÅÆĀĂĄẠẢẤẦẨẪẬẮẰẲẴẶ]" },
    { to: "c", from: "[ÇĆĈČ]" },
    { to: "d", from: "[ÐĎĐÞ]" },
    { to: "e", from: "[ÈÉÊËĒĔĖĘĚẸẺẼẾỀỂỄỆ]" },
    { to: "g", from: "[ĜĞĢǴ]" },
    { to: "h", from: "[ĤḦ]" },
    { to: "i", from: "[ÌÍÎÏĨĪĮİỈỊ]" },
    { to: "j", from: "[Ĵ]" },
    { to: "ij", from: "[Ĳ]" },
    { to: "k", from: "[Ķ]" },
    { to: "l", from: "[ĹĻĽŁ]" },
    { to: "m", from: "[Ḿ]" },
    { to: "n", from: "[ÑŃŅŇ]" },
    { to: "o", from: "[ÒÓÔÕÖØŌŎŐỌỎỐỒỔỖỘỚỜỞỠỢǪǬƠ]" },
    { to: "oe", from: "[Œ]" },
    { to: "p", from: "[ṕ]" },
    { to: "r", from: "[ŔŖŘ]" },
    { to: "s", from: "[ßŚŜŞŠ]" },
    { to: "t", from: "[ŢŤ]" },
    { to: "u", from: "[ÙÚÛÜŨŪŬŮŰŲỤỦỨỪỬỮỰƯ]" },
    { to: "w", from: "[ẂŴẀẄ]" },
    { to: "x", from: "[ẍ]" },
    { to: "y", from: "[ÝŶŸỲỴỶỸ]" },
    { to: "z", from: "[ŹŻŽ]" },
    { to: "-", from: "[·/_,:;']" },
  ];

  sets.forEach((set) => {
    text = text.replace(new RegExp(set.from, "gi"), set.to);
  });

  text = text
    .toString()
    .toLowerCase()
    .replace(/\s+/g, "-") // Replace spaces with -
    .replace(/&/g, "-and-") // Replace & with 'and'
    .replace(/[^\w\-]+/g, "") // Remove all non-word chars
    .replace(/\--+/g, "-") // Replace multiple - with single -
    .replace(/^-+/, "") // Trim - from start of text
    .replace(/-+$/, ""); // Trim - from end of text

  if (typeof separator !== "undefined" && separator !== "-") {
    text = text.replace(/-/g, separator);
  }

  return text;
}

const mobile = window.matchMedia("(max-width: 576px)");
const large_mobile = window.matchMedia("(max-width: 992px)");
const logo = document.getElementById("logo");
const sidenotes = document.getElementById("sidenotes");

function showLogoOnscroll() {
  if (!mobile.matches) {
    if (
      document.body.scrollTop > 20 ||
      document.documentElement.scrollTop > 20
    ) {
      logo.style.opacity = 1;
    } else {
      logo.style.opacity = 0;
    }
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  window.scroll({ top: 0, behavior: "smooth" });
}

// SIDENOTES

var sideNoteIsVisible = 0;

function showSidenote(e) {
  if (!mobile.matches) {
    var fn = e.target;
    var i = fn.id.match("[0-9]+")[0];
    sideNoteIsVisible = i;

    var s = document.querySelector("#sidefoot-" + i);
    s.style.display = "block";
    s.offsetHeight; // Recalc style, see stackoverlow link below
    s.style.opacity = 1;
    s.style.transform = "translateY(" + e.pageY + "px)";
    hideAllMenus();
  }
}

function hideSidenote(e) {
  sideNoteIsVisible = 0;
  var fn = e.target;
  var i = fn.id.match("[0-9]+")[0];
  var s = document.querySelector("#sidefoot-" + i);
  s.style.opacity = 0;
  setTimeout(() => {
    if (sideNoteIsVisible != i) s.style.display = "none";
  }, 300);
}

function placeSidenotes() {
  // copy footnotes to sidenotes
  var i = 1;
  var f, footn;

  while ((f = document.querySelector("#footnote-" + i))) {
    var clone = f.cloneNode(true);
    clone.id = "sidefoot-" + i;

    // Remove backlink icons
    try {
      var backlink = clone.getElementsByClassName("fn-backlink")[0];
      backlink.parentNode.removeChild(backlink);

      var backlink = clone.getElementsByClassName("fn-backlink")[0];
      backlink.parentNode.removeChild(backlink);
    } catch {}

    // We may have footnote-marker-1-1, footnote-marker-1-2, etc
    var k = 1;
    while (
      (footn = document.querySelector("#footnote-marker-" + i + "-" + k))
    ) {
      footn.onmouseenter = showSidenote;
      footn.onmouseleave = hideSidenote;
      k++;
    }

    // Insert in sidenotes
    sidenotes.appendChild(clone);
    i++;
  }
}
placeSidenotes();

// MENUS DISPLAY

// Hold the list visible even if mouse isn't there
var holdVisible = null;
// These should be something like a class property but shared among all
// instances.

class Menu {
  constructor(
    menu,
    menu_button,
    isSubMenu = false,
    onshow = null,
    onhide = null,
  ) {
    this.menu = menu;
    this.menu_button = menu_button;
    this.isSubMenu = isSubMenu;
    this.onshow = onshow;
    this.onhide = onhide;

    // The list is visible
    this.isVisible = false;
    this.alwaysVisible = false;

    this.menu_button.onmouseenter = this.showMenu.bind(this);
    this.menu_button.onclick = this.insideClick.bind(this);
  }

  showMenu() {
    if (holdVisible == null || holdVisible == this) {
      this.isVisible = true;
      hideOtherMenus(this);
      this.menu.ontransitionend = null;
      this.menu.style.display = "block";
      // Force style recalc. See
      // stackoverflow.com/questions/39548684/css-transition-opacity-is-not-working-where-element-had-displaynone-then-change/39548790#39548790
      this.menu.offsetHeight;
      this.menu.style.opacity = 1;

      if (this.onshow) this.onshow();
    }
  }

  hideMenus() {
    if (
      !this.alwaysVisible &&
      holdVisible != this &&
      !(this.isSubMenu && holdVisible != null)
    ) {
      this.isVisible = false;
      this.menu.style.opacity = 0;
      this.menu.onmouseenter = null;
      // Wait for transition to finish before taking section list out of
      // screen (we must set display to none otherwise it's still
      // clickable)

      this.menu.ontransitionend = () => {
        if (!this.isVisible) this.menu.style.display = "none";
      };

      if (this.onhide) this.onhide();
    }
  }

  insideClick() {
    if (holdVisible == this) {
      holdVisible = null;
      this.closedByButton = true;
      this.hideMenus(null);
    } else {
      holdVisible = this;
      this.showMenu(null);
    }
  }
}

const bottom_bar = document.getElementById("bottom-bar");
const top_button = document.getElementById("top-button");
const main_icon = document.getElementById("main-icon");

const submenu = new Menu(
  document.getElementById("submenu"),
  document.getElementById("main-button"),
  true,
  () => {
    // onshow
    main_icon.style.filter = "opacity(50.7%)";
    main_icon.style.animationPlayState = "running";
    logo.ontransitionend = null;
    logo.style.display = "block";
    logo.offsetHeight; // Recalc style, see comment above
    logo.style.opacity = "1";
  },
  () => {
    // onhide
    main_icon.style.filter = "none";
    logo.ontransitionend = () => {
      logo.style.display = "none";
    };
    logo.style.opacity = "0";
    main_icon.style.animationPlayState = "paused";
  },
);

var menu_list = [];

var menus_elements = [
  document.getElementById("menu-sections"),
  document.getElementById("menu-pdfs"),
  document.getElementById("menu-languages"),
];
var menu_buttons_elements = [
  document.getElementById("sections-button"),
  document.getElementById("pdf-button"),
  document.getElementById("languages-button"),
];

function build_menu_list() {
  for (var i = 0; i < menus_elements.length; i++) {
    if (menus_elements[i] && menu_buttons_elements[i]) {
      menu_list.push(new Menu(menus_elements[i], menu_buttons_elements[i]));
    }
  }
}

function calc_menu_heights() {
  var k = 1;
  if (large_mobile.matches) {
    if (mobile.matches) k++;
    for (var i = menus_elements.length - 1; i >= 0; i--) {
      if (menus_elements[i]) k++;
    }
    for (var i = menus_elements.length - 1; i >= 0; i--) {
      if (menus_elements[i])
        menus_elements[i].style.bottom = "calc(44px*" + k + ")";
    }
  } else {
    for (var i = menus_elements.length - 1; i >= 0; i--) {
      if (menus_elements[i]) {
        menus_elements[i].style.bottom = "calc(44px*" + k + ")";
        k++;
      }
    }
  }
}

build_menu_list();
calc_menu_heights();

function hideOtherMenus(menu, include_submenu = false) {
  for (const m of menu_list) {
    if (m != menu && m.isVisible) {
      m.hideMenus();
    }
  }
  if (include_submenu) submenu.hideMenus();
}

function hideMenus() {
  hideOtherMenus(null);
}

function hideAllMenus() {
  hideOtherMenus(null, true);
}

function outsideClick(e) {
  if (!bottom_bar.contains(e.target)) {
    holdVisible = null;
    hideAllMenus();
  }
}

function detectEsc(e) {
  if (e.keyCode == 27) {
    holdVisible = null;
    hideAllMenus();
  }
}

top_button.onmouseenter = hideMenus;
window.addEventListener("click", outsideClick);
window.onkeydown = detectEsc;

// DETECTING MEDIA SIZE CHANGES

function handleMobileChange(e) {
  // Check if the media query is true
  if (e.matches) {
    bottom_bar.onmouseleave = hideAllMenus;
    submenu.alwaysVisible = false;
    hideAllMenus();
  } else {
    bottom_bar.onmouseleave = hideMenus;
    submenu.alwaysVisible = true;
    hideAllMenus();
    submenu.showMenu();
  }
  calc_menu_heights();
}

// Register event listener
mobile.addListener(handleMobileChange);
large_mobile.addListener(calc_menu_heights);

// Initial check
handleMobileChange(mobile);

// BUILD SECTIONS MENU
var listOfSections = [null];
var listOfSectionTitles = [null];
var scrollHandlerWait = false;
var currentSection = 0;

function displaySections() {
  // Gather all sections
  var i = 1;
  const sections = document.getElementsByClassName("text-section");
  const menu = document.getElementById("menu-sections");

  // If there are none, hide icons
  // If there are no sections, probably there is no pdf page...
  // TODO: remove when using CMS, replace by something in the backend
  if (sections.length == 0) {
    document.getElementById("sections-button").style.display = "none";
    return;
  }

  for (const s of sections) {
    var new_p = document.createElement("p");
    var new_a = document.createElement("a");

    // Disregard footnote, if there is one
    var fn;
    if ((fn = s.getElementsByClassName("footnoteref")).length > 0) {
      new_a.innerText = s.innerText.replace(fn[0].innerText, "");
    } else new_a.innerText = s.innerText;

    s.id = slugify(new_a.innerText, "-");
    if (s.id.length < 5) s.id = "secao-" + s.id;

    new_p.appendChild(new_a);
    new_a.href = "#" + s.id;
    new_a.classList.add("section-menu-item");
    new_a.id = "section-menu-item-" + i;
    menu.appendChild(new_p);
    i++;

    // Keep list of references to the sections
    listOfSections.push(s);
    listOfSectionTitles.push(new_a);
    // Add scroll handler for highlighting the current section
    window.addEventListener("scroll", scrollHandler, { passive: true });
  }
}

// Detect current section and highlight it's title in the menu
function calcCurrentSection() {
  var y,
    goingUp = -1;
  var i = currentSection;
  var screenHeight =
    window.innerHeight ||
    document.documentElement.clientHeight ||
    document.body.clientHeight;

  while (i >= 0 && i <= listOfSections.length) {
    if (i >= listOfSections.length) y = Infinity;
    else if (i) y = listOfSections[i].getBoundingClientRect().y;
    else y = -Infinity;

    if (y < screenHeight / 2) {
      if (goingUp == 0) break;
      i++;
      goingUp = 1;
    }

    if (y >= screenHeight / 2) {
      i--;
      if (goingUp == 1) {
        break;
      }
      goingUp = 0;
    }
  }

  if (i != currentSection) {
    if (currentSection)
      listOfSectionTitles[currentSection].classList.remove("current-tab");
    if (i) listOfSectionTitles[i].classList.add("current-tab");
  }

  currentSection = i;
}

function scrollHandler() {
  if (!scrollHandlerWait) {
    scrollHandlerWait = true;
    setTimeout(function () {
      calcCurrentSection();
      showLogoOnscroll();
      scrollHandlerWait = false;
    }, 300);
  }
}

displaySections();

// OPEN EXTERNAL LINKS IN NEW TAB
function newTabOnLink() {
  var links = document.links;
  for (var i = 0; i < links.length; i++) {
    if (!links[i].href.match(/neblina.xyz|localhost|file:/i)) {
      links[i].target = "_blank";
    }
  }
}
newTabOnLink();
