from django.core.management.base import BaseCommand
from django.conf import settings
import subprocess


class Command(BaseCommand):
    help = "Compile project's sass files into css"

    def add_arguments(self, parser):
        parser.add_argument("watch", type=str, nargs="?", default="")

    def handle(self, *args, **options):
        src = str(settings.BASE_DIR / "pages/static/pages/scss/")
        dest = str(settings.BASE_DIR / "pages/static/pages/css/")
        watch = []
        if options["watch"] == "watch":
            watch = ["--watch"]

        try:
            subprocess.run(["sass"] + watch + [src + ":" + dest, "-s", "compressed"])
        except KeyboardInterrupt:
            print()
            exit()
