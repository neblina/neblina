from django.core.management.commands import makemessages
from django.conf import settings
from django.utils.translation import to_locale
import os


class Command(makemessages.Command):
    """Warning: hacky solution ahead"""

    help = "Build gettext messages putting the 'frontend' translations "
    "(the ones seen by users) at the top level locale dir, and the rest "
    "at app level locale dir. You can use any makemessages command here, "
    "except for --extension. This also ignores the venv folder by "
    "default, if used without --no-override."

    def safe_mv(self, src, dest):
        suffix = 1
        error = None
        for _ in range(10):
            try:
                os.rename(src, dest + str(suffix))
                error = None
                break
            except OSError as e:
                error = e
                suffix += 1

        if error:
            raise error

        return dest + str(suffix)

    def add_arguments(self, parser):
        parser.add_argument(
            "--no-override",
            action="store_true",
            help="Use default makemessages command",
        )
        parser.add_argument(
            "--build-all",
            action="store_true",
            help="Build locale folder for all languages in settings.LANGUAGES",
        )
        return super().add_arguments(parser)

    def handle(self, *args, **options):
        if options["no_override"]:
            return super().handle(*args, **options)

        print(
            "Using custom makemessages command defined in "
            "pages/management. To run default makemessages command, "
            "run makemessages --no-override."
        )

        locale_folder = "locale"
        src = "pages/" + locale_folder
        dst = "pages/tmp-locale"

        if not options["ignore_patterns"]:
            options["ignore_patterns"].append("venv")

        run_all = False
        if options["build_all"]:
            options['locale'] += [to_locale(l[0]) for l in settings.LANGUAGES]
            run_all = options['all']
            # --all makes the command ignore given locales
            options['all'] = False
            try:
                os.mkdir(src)
            except FileExistsError:
                pass
            try:
                os.mkdir(locale_folder)
            except FileExistsError:
                pass

        # Build considering .py files. This will use the app locale dir
        options["extensions"] = ["py"]
        super().handle(*args, **options)
        if run_all:
            options['all'] = True
            super().handle(*args, **options)
            options['all'] = False

        # Change name of app's locale dir, forcing the use of top level
        # locale dir
        new_name = self.safe_mv(src, dst)
        options["extensions"] = ["html", "txt"]
        super().handle(*args, **options)
        if run_all:
            options['all'] = True
            super().handle(*args, **options)
            options['all'] = False
        os.rename(new_name, src)

