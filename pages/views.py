from django.shortcuts import render
from django.http import Http404
from django.conf import settings
from django.utils.translation import trans_real, activate
from django.views import View
from django.core.exceptions import ObjectDoesNotExist

from .models import Page, Configuration


class PageView(View):
    def get_page_from_slug(self, slug):
        try:
            page = Page.objects.get(translations__slug=slug)
        except ObjectDoesNotExist:
            return None
        language = page.translations.get(slug=slug).language_code
        page.set_current_language(language)

        return page

    def get_display_languages(self):
        """
        Example: if brazilian portuguese is the only portuguese version
        of this page, we don't want to display "Brazilian portuguese" in
        the site, rather, we would like to display only "portuguese".
        Thus, this function builds a list of "display languages": if
        there is no other cultural variation of a language in available
        languages, we include that language without cultural variation.
        """
        result = []
        base_langs = list(map(lambda x: x.split("-", 1)[0], self.available_languages))
        for i in range(len(self.available_languages)):
            if base_langs.count(base_langs[i]) < 2:
                result.append(base_langs[i])
            else:
                result.append(self.available_languages[i])

        return result

    def apply_language_preference(self, page):
        for lang in self.lang_prefs:
            target_lang = page.has_translation(lang, loose=True, draft=self.auth)
            if target_lang:
                page.set_current_language(target_lang)
                break
        else:
            al = page.get_available_languages(draft=self.auth)
            if not al:
                return None
            page.set_current_language(al[0])

        return page

    def get_menu_pages(self):
        pages = Page.objects.filter(translations__visible_in_menu=True).distinct()
        pages = map(lambda p: self.apply_language_preference(p), pages)
        return [p for p in pages if p]

    def parse_accept_language(self, exclude=None):
        try:
            prefs = self.request.headers["Accept-Language"]
        except KeyError:
            return ()
        prefs = map(lambda k: k[0], trans_real.parse_accept_lang_header(prefs))
        prefs = filter(lambda x: x not in exclude, prefs)

        return list(prefs)

    def get_language_preferences(self):
        """
        Receives a request and the language of the page (optionally),
        returns list with the languages in order of preference. Order of
        priority:
         - Language of the page, if given
         - Language in cookie
         - Language in Accept-Languages header
         - Default fallbacks in PARLER_LANGUAGES
        """
        lang_prefs = []
        if self.language:
            lang_prefs += [self.language]
        try:
            lang_prefs += [self.request.COOKIES["django_language"]]
        except KeyError:
            pass

        lang_prefs += list(self.parse_accept_language(exclude=lang_prefs))

        try:
            lang_prefs += list(
                filter(
                    lambda x: x not in lang_prefs,
                    settings.PARLER_LANGUAGES["default"]["fallbacks"],
                )
            )
        except (KeyError, AttributeError):
            pass

        return lang_prefs

    def setup(self, request, *args, **kwargs):
        self.auth = request.user.is_authenticated
        self.request = request
        self.page = None
        self.language = None
        self.available_languages = None
        self.lang_prefs = None
        super().setup(request, *args, **kwargs)

    def dispatch(self, request, *args, **kwargs):
        self.page = self.get_page_from_slug(kwargs["slug"])

        if not self.page or (not self.auth and self.page.draft):
            raise Http404

        self.language = self.page.get_current_language()
        self.lang_prefs = self.get_language_preferences()

        return self.base(request)

    def base(self, request, template="pages/page.html"):
        """Expect page with the correct current language already set"""
        self.available_languages = list(self.page.get_available_languages(self.auth))

        context = {
            "page": self.page,
            "language": self.language,
            "menu_pages": self.get_menu_pages(),
            "about_page": self.apply_language_preference(
                Configuration.get_solo().about,
            ),
            "available_langs": self.available_languages,
            "display_langs": self.get_display_languages(),
            "cache_timeout": getattr(
                settings,
                'CACHE_MIDDLEWARE_SECONDS',
                60 * 60 * 24,
            ),
            "auth": self.auth,
        }

        self.page.views += 1
        self.page.save()

        activate(self.language)
        return render(request, template, context)


class HomePageView(PageView):
    def dispatch(self, request):
        self.page = Configuration.get_solo().homepage

        if not self.page:
            raise Http404

        self.lang_prefs = self.get_language_preferences()
        self.page = self.apply_language_preference(self.page)

        if not self.page:
            raise Http404

        return self.base(request)
