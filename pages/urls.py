from django.urls import path, re_path
from django.shortcuts import redirect

from .views import PageView, HomePageView

urlpatterns = [
    re_path(
        "(?P<slug>masterclass|sobre|olha-como).html",
        lambda request, **d: redirect("/" + d['slug'], permanent=True,),
    ),
    path("<str:slug>", PageView.as_view(), name="page"),
    re_path(r"^$", HomePageView.as_view(), name="homepage"),
]
