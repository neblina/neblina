from .models import Page
from .utils import _parse_url, slugify_filename

def get_filename(filename, request):
    name = slugify_filename(filename, allow_unicode=True)
    try:
        page_id = _parse_url(request.headers['referer'])
    except (KeyError, ValueError, AttributeError):
        return name
    page = Page.objects.get(id=page_id)
    slug = page.safe_translation_getter('slug', language_code=page.default_language)
    if slug:
        folder = slug + '/'
    else:
        folder = ''
    return folder + name

