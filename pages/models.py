from django.conf import settings
from django.core.cache import cache
from django.db import models
from django.forms.models import model_to_dict
from django.utils.timezone import now
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _
from parler.models import TranslatableModel, TranslatedFields
from PIL import Image
from solo.models import SingletonModel

from .fields import RichTextUploadingField
from .utils import slugify_filename


# Customize boolean methods due to bug in django admin
class ImageFieldFile(models.fields.files.ImageFieldFile):
    def __bool__(self):
        return str(self) != "" and str(self) != "False"


class FieldFile(models.fields.files.FieldFile):
    def __bool__(self):
        return str(self) != "" and str(self) != "False"


class FileField(models.FileField):
    attr_class = FieldFile


class ImageField(models.ImageField):
    attr_class = ImageFieldFile


def _get_extra_styles():
    """
    Query the database for extra styles, defined by user in
    Configurations
    """
    styles = Configuration.get_solo().extra_styles.all()
    ret = []
    for style in styles:
        d = model_to_dict(style)
        d["attributes"] = {"class": d["css_class"]}
        del d["css_class"]
        d = {k: d[k] for k in ("name", "element", "attributes")}
        ret.append(d)
    return ret


class Page(TranslatableModel):
    def get_filename(page, filename):
        name = slugify_filename(filename, allow_unicode=True)
        slug = page.master.safe_translation_getter(
            "slug", language_code=page.master.default_language
        )
        if slug:
            folder = slug + "/"
        else:
            folder = ""
        return folder + name

    translations = TranslatedFields(
        # meta data
        pub_date=models.DateTimeField(_("Date of publication"), default=now),
        views=models.IntegerField(
            _("Number of views"),
            help_text=_(
                "Each time a user views this page, this number increases. The"
                " same user reloading the page repeatedly will cause this"
                " number to increase."
            ),
            default=0,
        ),
        draft=models.BooleanField(
            _("Is this still a draft?"),
            help_text=_(
                "Check this if this page is still a work in progress. If so, it"
                " will only appear for logged in administrators. When a page is"
                " not a draft, it is published."
            ),
            default=True,
        ),
        visible_in_menu=models.BooleanField(
            _("Visibe in menu?"),
            help_text=_(
                "This option determines whether this page is acessible in the"
                " dropdown menu in the header of the site."
            ),
            default=True,
        ),
        # html
        title=models.CharField(
            _("Main title"),
            help_text=_(
                "Title of the article. You should also include this in the text editor."
            ),
            max_length=500,
        ),
        page_title=models.CharField(
            _("Page title"),
            help_text=_(
                "Title of this page. Appears in the browser's tab. Leave it"
                " empty if you wish to generate it automatically. Automatic"
                ' generation yields "Title | Neblina" (example: "Masterclass de'
                ' fim de mundo | Neblina").'
            ),
            max_length=500,
            default="",
            blank=True,
        ),
        description=models.CharField(
            _("Short description"),
            help_text=_(
                "Short description of the page. This is used by google when"
                " displaying search results (not to be confused with"
                ' "og:description").'
            ),
            max_length=700,
        ),
        # Actual text
        text=RichTextUploadingField(
            _("Text"),
            help_text=_(
                'Use the "styles" to write the title, subtitle, section'
                " title, etc. To remove a style from a line of text, select it"
                " and click the style you whish to remove. The spacing is not"
                " exactly as it will show in the real page: visit the site to"
                " see how it's looking. When inserting an image, don't use the"
                " editor to define it's size, it is already defined with css."
                " For inline images, use the class img-inline. For images"
                " before the title, use the class img-top. Please save the"
                " page before uploading any images."
            ),
            default="",
            blank=True,
            external_plugin_resources=settings.CKEDITOR_PLUGIN_RESOURCES,
            config_name="pages",
            get_extra_styles=_get_extra_styles,
        ),
        slug=models.SlugField(
            _("Slug"),
            help_text=_(
                "This is what will be used to build the url to this page. It"
                " should be a unique identifier for the page."
            ),
            null=False,
            unique=True,
        ),
        html_title=models.CharField(
            _("Formatted title"),
            help_text=_(
                "Formatted version of the title. Use this if the title of this"
                " article should be formatted (if it contains italic text,"
                " superscript, etc). Use html to do the formatting. This will"
                " be displayed in the header menu of the site. If left empty,"
                " will automatically be filled with the main title of the page."
            ),
            max_length=1000,
            blank=True,
        ),
        # OG meta properties
        og_description=models.CharField(
            _("OGP Description"),
            help_text=_(
                "Short description of this page for the OGP. If left empty,"
                " will be automatically filled with the shord description of"
                " this page."
            ),
            max_length=700,
            blank=True,
        ),
        og_title=models.CharField(
            _("OGP Title"),
            help_text=_(
                "Title of this page for the OGP. If left empty, will be"
                " automatically filled with the main title of the page"
            ),
            max_length=200,
            blank=True,
        ),
        og_type=models.CharField(
            _("OGP Type"),
            help_text=_('Type of this page for the OGP. By default, "article"'),
            max_length=50,
            default="article",
        ),
        og_image=ImageField(
            _("OGP Image"),
            upload_to=get_filename,
            help_text=_(
                "This should be a compressed image with size of 1200x630 pixels."
            ),
            max_length=100,
            default="",
            blank=True,
        ),
        # PDF files
        pdf_A4=FileField(
            _("PDF version, A4"),
            upload_to=get_filename,
            help_text=_(
                "This is a PDF version of the page, formatted for printing. It"
                " should be in landscape orientation and contain two pages in"
                " each sheet. The size of the sheet should be A4."
            ),
            max_length=100,
            default="",
            blank=True,
        ),
        pdf_A5=FileField(
            _("PDF version, A5"),
            upload_to=get_filename,
            help_text=_(
                "This is a PDF version of the page, formatted for reading in a"
                " cellphone or tablet. It should be in portrait orientation and"
                " contain one page per sheet. The size of the sheet should"
                " be A5."
            ),
            max_length=100,
            default="",
            blank=True,
        ),
    )

    custom_css = models.TextField(
        _("Custom CSS"),
        help_text=_(
            "Write your own css rules here. This is the same for all"
            " translations of this page."
        ),
        blank=True,
    )
    order = models.PositiveIntegerField(default=0, blank=False, null=False)
    default_language = models.CharField(
        _("Default language"),
        help_text=_(
            "Default language for the page. It is automatically set as the "
            "first language to be created."
        ),
        max_length=10,
        blank=True,
        null=True,
    )

    # If fields in keys are empty, they will be filled with the value of
    # the field in values upon save.
    fallback_fields = {
        "page_title": ("{t} | Neblina", {"t": "title"}),
        "og_title": "title",
        "html_title": "title",
        "og_description": "description",
    }

    def save(self, *args, **kwargs):
        for field_name, fb_data in self.fallback_fields.items():
            field = getattr(self, field_name)
            if field == "":
                if type(fb_data) == tuple:
                    fmt = {}
                    for var, fb_field_name in fb_data[1].items():
                        fmt[var] = getattr(self, fb_field_name)
                    fb_data = fb_data[0].format(**fmt)
                else:
                    fb_data = getattr(self, fb_data)

                setattr(self, field_name, fb_data)

        cache.clear()

        if not self.default_language:
            self.default_language = self.language_code
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title

    @property
    def og_image_type(self):
        if self.og_image:
            i = Image.open(self.og_image)
            return Image.MIME[i.format]
        return None

    def has_translation(
        self, language_code=None, related_name=None, loose=False, draft=False
    ):
        """Implement a loose variant of has_translation, which allows
        for different cultural variants to match. In this case, the
        matched translation is returned. If the loose mode is used, then
        the draft option is considered. If true, will also return
        language versions which are still drafts.
        """
        if not loose:
            return super().has_translation(
                language_code=language_code, related_name=related_name
            )
        # Sorting to make sure to return specific match before returning
        # loose match
        al = list(self.get_available_languages(draft))
        al.sort(key=len, reverse=True)

        for target in language_code, language_code.split("-", 1)[0]:
            for existing in al:
                if target == existing:
                    return existing
            for existing in al:
                if target == existing.split("-", 1)[0]:
                    return existing
        return False

    def get_available_languages(self, draft=True):
        if draft:
            return super().get_available_languages()

        return self.translations.filter(draft=False).values_list(
            "language_code", flat=True
        )

    class Meta:
        verbose_name = _("Page")
        verbose_name_plural = _("Pages")
        ordering = ["order"]


class Configuration(SingletonModel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = gettext("Configurations")

    homepage = models.ForeignKey(
        Page,
        verbose_name=_("Homepage"),
        on_delete=models.SET_NULL,
        related_name="+",
        help_text=_("Which page is to be shown if no slug is given?"),
        null=True,
    )

    about = models.ForeignKey(
        Page,
        verbose_name=_("About"),
        on_delete=models.SET_NULL,
        related_name="+",
        help_text=_('Which page is the "about" page?'),
        null=True,
    )

    def __str__(self):
        return gettext("Configurations")

    def save(self, *args, **kwargs):
        cache.clear()
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = _("Configuration")


class ExtraStyle(models.Model):
    name = models.CharField(
        _("Name"),
        max_length=20,
        blank=False,
    )

    element = models.CharField(
        _("Element"),
        help_text=_(
            "HTML element to be used for this style. Examples: 'p', 'h1',"
            " 'div', etc."
        ),
        max_length=20,
    )

    css_class = models.CharField(
        _("CSS class"),
        help_text=_(
            "CSS class to use with this style. This class can later be"
            " styled with the 'Custom CSS' option of the page"
        ),
        max_length=50,
    )

    configuration = models.ForeignKey(
        Configuration,
        on_delete=models.SET_NULL,
        related_name="extra_styles",
        null=True,
    )

    def __str__(self):
        return self.name


class Documentation(TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(
            _("Title"),
            max_length=200,
        ),
        content=RichTextUploadingField(
            _("Content"),
            default="",
            blank=True,
            external_plugin_resources=settings.CKEDITOR_PLUGIN_RESOURCES,
            config_name="documentation",
        ),
    )
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("Guide or documentation")
        verbose_name_plural = _("Guides and documentation")
        ordering = ["order"]
