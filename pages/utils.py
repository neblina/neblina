import re, os
from django.utils.text import slugify
from ckeditor_uploader.utils import get_random_string

def slugify_filename(filename, allow_unicode=False):
    name, ext = os.path.splitext(filename)
    slugified = slugify(name, allow_unicode=True)
    if not slugified:
        slugified = get_random_string()
    return slugified + ext

page_id_re = re.compile(r'.*pages/page/(\d+)/.*')
def _parse_url(url):
    m = page_id_re.match(url)
    page_id = int(m.group(1))
    return page_id
