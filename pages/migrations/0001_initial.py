# Generated by Django 4.0.3 on 2022-05-25 01:13

import ckeditor_uploader.fields
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import pages.models
import parler.fields
import parler.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Documentation',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order', models.PositiveIntegerField(default=0)),
            ],
            options={
                'verbose_name': 'Guide or documentation',
                'verbose_name_plural': 'Guides and documentation',
                'ordering': ['order'],
            },
            bases=(parler.models.TranslatableModelMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('custom_css', models.TextField(blank=True, help_text="Write your own css rules here. This is the same for all translations of this page. It doesn't show in the text editor: you must open the page to visualize it.", verbose_name='Custom CSS')),
                ('order', models.PositiveIntegerField(default=0)),
                ('default_language', models.CharField(blank=True, help_text='Default language for the page. It is automatically set as the first language to be created.', max_length=10, null=True, verbose_name='Default language')),
            ],
            options={
                'verbose_name': 'Page',
                'verbose_name_plural': 'Pages',
                'ordering': ['order'],
            },
            bases=(parler.models.TranslatableModelMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Configuration',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('about', models.ForeignKey(help_text='Which page is the "about" page?', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='pages.page', verbose_name='About')),
                ('homepage', models.ForeignKey(help_text='Which page is to be shown if no slug is given?', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='pages.page', verbose_name='Homepage')),
            ],
            options={
                'verbose_name': 'Configuration',
            },
        ),
        migrations.CreateModel(
            name='PageTranslation',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('language_code', models.CharField(db_index=True, max_length=15, verbose_name='Language')),
                ('pub_date', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date of publication')),
                ('views', models.IntegerField(default=0, help_text='Each time a user views this page, this number increases. The same user reloading the page repeatedly will cause this number to increase.', verbose_name='Number of views')),
                ('draft', models.BooleanField(default=True, help_text='Check this if this page is still a work in progress. If so, it will only appear for logged in administrators. When a page is not a draft, it is published.', verbose_name='Is this still a draft?')),
                ('visible_in_menu', models.BooleanField(default=True, help_text='This option determines whether this page is acessible in the dropdown menu in the header of the site.', verbose_name='Visibe in menu?')),
                ('title', models.CharField(help_text='Title of the article. You should also include this in the text editor.', max_length=500, verbose_name='Main title')),
                ('page_title', models.CharField(blank=True, default='', help_text='Title of this page. Appears in the browser\'s tab. Leave it empty if you wish to generate it automatically. Automatic generation yields "Title | Neblina" (example: "Masterclass de fim de mundo | Neblina").', max_length=500, verbose_name='Page title')),
                ('description', models.CharField(help_text='Short description of the page. This is used by google when displaying search results (not to be confused with "og:description").', max_length=700, verbose_name='Short description')),
                ('text', ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', help_text='Use the "styles" to write the title, subtitle, section title, etc. To remove a style from a line of text, select it and click the style you whish to remove. The spacing is not exactly as it will show in the real page: visit the site to see how it\'s looking. When inserting an image, don\'t use the editor to define it\'s size, it is already defined with css. For inline images, use the class img-inline. For images before the title, use the class img-top. Please save the page before uploading any images.', verbose_name='Text')),
                ('slug', models.SlugField(help_text='This is what will be used to build the url to this page. It should be a unique identifier for the page.', unique=True, verbose_name='Slug')),
                ('html_title', models.CharField(blank=True, help_text='Formatted version of the title. Use this if the title of this article should be formatted (if it contains italic text, superscript, etc). Use html to do the formatting. This will be displayed in the header menu of the site. If left empty, will automatically be filled with the main title of the page.', max_length=1000, verbose_name='Formatted title')),
                ('og_description', models.CharField(blank=True, help_text='Short description of this page for the OGP. If left empty, will be automatically filled with the shord description of this page.', max_length=700, verbose_name='OGP Description')),
                ('og_title', models.CharField(blank=True, help_text='Title of this page for the OGP. If left empty, will be automatically filled with the main title of the page', max_length=200, verbose_name='OGP Title')),
                ('og_type', models.CharField(default='article', help_text='Type of this page for the OGP. By default, "article"', max_length=50, verbose_name='OGP Type')),
                ('og_image', pages.models.ImageField(blank=True, default='', help_text='This should be a compressed image with size of 1200x630 pixels.', upload_to=pages.models.Page.get_filename, verbose_name='OGP Image')),
                ('pdf_A4', pages.models.FileField(blank=True, default='', help_text='This is a PDF version of the page, formatted for printing. It should be in landscape orientation and contain two pages in each sheet. The size of the sheet should be A4.', upload_to=pages.models.Page.get_filename, verbose_name='PDF version, A4')),
                ('pdf_A5', pages.models.FileField(blank=True, default='', help_text='This is a PDF version of the page, formatted for reading in a cellphone or tablet. It should be in portrait orientation and contain one page per sheet. The size of the sheet should be A5.', upload_to=pages.models.Page.get_filename, verbose_name='PDF version, A5')),
                ('master', parler.fields.TranslationsForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='translations', to='pages.page')),
            ],
            options={
                'verbose_name': 'Page Translation',
                'db_table': 'pages_page_translation',
                'db_tablespace': '',
                'managed': True,
                'default_permissions': (),
                'unique_together': {('language_code', 'master')},
            },
            bases=(parler.models.TranslatedFieldsModelMixin, models.Model),
        ),
        migrations.CreateModel(
            name='DocumentationTranslation',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('language_code', models.CharField(db_index=True, max_length=15, verbose_name='Language')),
                ('title', models.CharField(max_length=200, verbose_name='Title')),
                ('content', ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', verbose_name='Content')),
                ('master', parler.fields.TranslationsForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='translations', to='pages.documentation')),
            ],
            options={
                'verbose_name': 'Guide or documentation Translation',
                'db_table': 'pages_documentation_translation',
                'db_tablespace': '',
                'managed': True,
                'default_permissions': (),
                'unique_together': {('language_code', 'master')},
            },
            bases=(parler.models.TranslatedFieldsModelMixin, models.Model),
        ),
    ]
