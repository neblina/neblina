from django import template
from django.urls import reverse

register = template.Library()


@register.simple_tag
def absolute_uri(request, path):
    return request.build_absolute_uri(path)


@register.simple_tag
def page_absolute_uri(request, page, lang_code=None):
    if lang_code:
        slug = page.safe_translation_getter("slug", language_code=lang_code)
    else:
        slug = page.slug
    return request.build_absolute_uri(reverse("page", kwargs={"slug": slug}))


@register.simple_tag
def zip_lists(a, b):
    return zip(a, b)
