from polib import pofile
from django.contrib import admin
from django.contrib.admin.utils import quote
from django.contrib import messages
from django.core.exceptions import ValidationError
from django.utils.translation import get_language_info, gettext as _
from django.utils.text import format_lazy
from django.utils.safestring import mark_safe
from django.utils.html import conditional_escape, escape
from django.urls import reverse
from django.conf import settings
from parler.admin import TranslatableAdmin
from parler.forms import TranslatableModelForm
from solo.admin import SingletonModelAdmin
from adminsortable2.admin import SortableAdminMixin
from codemirror import CodeMirrorTextarea
from rosetta.poutil import find_pos
from ckeditor_uploader.fields import RichTextUploadingField
from .models import Page, Configuration, Documentation, ExtraStyle

admin.site.index_title = _("Neblina")
admin.site.site_header = _("Neblina")
admin.site.site_title = _("Administration")

class PageAdminForm(TranslatableModelForm):
    def _get_duplicate_slug_message(self):
        slug = self.instance.slug
        dup = self.instance.translations.filter(slug=slug)
        if dup:
            return format_lazy(
                _("This slug is the same in the {l} version of this page."),
                l=get_language_info(dup[0].language_code)["name_translated"],
            )

        dup = Page.objects.filter(translations__slug=slug)
        if dup:
            return format_lazy(
                _("This slug is the same in the {l} version of page {p}."),
                l=get_language_info(dup[0].translations.get(slug=slug).language_code)[
                    "name_translated"
                ],
                p=dup[0].html_title,
            )
        return None

    def _update_errors(self, errors):
        """Django-parler calls validate_unique() twice, resulting in a
        duplicate error message. This method undoes this duplication and
        improves the error message to specify which object are causing
        the duplication
        """
        if not hasattr(errors, "error_dict"):
            return super()._update_errors(errors)

        deduplicated_error_messages = {}

        for field, messages in errors.error_dict.items():
            for message in messages:
                if self.has_error(field, message.code):
                    continue
                deduplicated_error_messages.setdefault(field, [])
                deduplicated_error_messages[field].append(message)
                if message.code == "unique":
                    new_message = self._get_duplicate_slug_message()
                    if new_message:
                        message.message = new_message

        return super()._update_errors(ValidationError(deduplicated_error_messages))

    class Meta:
        model = Page
        exclude = ["order"]
        widgets = {
            "custom_css": CodeMirrorTextarea(
                mode="css",
                # theme='cobalt',
                config={"fixedGutter": True},
            )
        }


class PageAdmin(SortableAdminMixin, TranslatableAdmin):
    list_display = ("title", "language_column")

    form = PageAdminForm

    # Use method instead of attribute due to django-parler
    def get_prepopulated_fields(self, request, obj=None):
        return {"slug": ("title",)}

    fieldsets = [
        (None, {"fields": ["title", "slug", "draft", "visible_in_menu"]}),
        (_("Text"), {"fields": ["text"], "classes": ("wide",)}),
        (
            _("Metadata"),
            {
                "fields": [
                    "description",
                    "page_title",
                    "pub_date",
                    "views",
                ],
                "classes": ("collapse",),
            },
        ),
        (
            _("Aditional formatting"),
            {
                "fields": [
                    "html_title",
                    "custom_css",
                ],
                "classes": ("collapse",),
            },
        ),
        (
            _("Open Graph Protocol"),
            {
                "fields": [
                    "og_description",
                    "og_title",
                    "og_type",
                    "og_image",
                ],
                "classes": ("collapse",),
                "description": _(
                    "These fields will constitute the open graph protocol"
                    " header section of the page. Visit <a"
                    ' href="https://ogp.me">ogp.me</a> for more information.'
                    " Using these is good for SEO. These define what is shown"
                    " when you send a link of this page in social media."
                ),
            },
        ),
        (
            _("PDF versions"),
            {
                "fields": [
                    "pdf_A4",
                    "pdf_A5",
                ],
                "classes": ("collapse",),
            },
        ),
    ]

    @admin.display(
        description=mark_safe(
            '{name} <span class="explanation">({expl})</span>'.format(
                name=_("Translations"),
                expl=_("Highlighted translations are published"),
            )
        )
    )
    def language_column(self, object):
        return super().language_column(object)

    def _languages_column(self, object, all_languages=None, span_classes=""):
        active_languages = self.get_available_languages(object)
        if all_languages is None:
            all_languages = active_languages

        draft_languages = object.translations.filter(draft=False).values_list(
            "language_code", flat=True
        )

        current_language = object.get_current_language()
        buttons = []
        opts = self.opts
        for code in all_languages or active_languages:
            classes = ["lang-code"]
            if code in active_languages:
                classes.append("active")
            else:
                classes.append("untranslated")
            if code == current_language:
                classes.append("current")
            if code in draft_languages:
                classes.append("published")

            info = opts.app_label, opts.model_name
            admin_url = reverse(
                "admin:{}_{}_change".format(*info),
                args=(quote(object.pk),),
                current_app=self.admin_site.name,
            )
            buttons.append(
                '<a class="{classes}"'
                ' href="{href}?language={language_code}">{title}</a>'.format(
                    language_code=code,
                    classes=" ".join(classes),
                    href=escape(admin_url),
                    title=conditional_escape(self.get_language_short_title(code)),
                )
            )
        return '<span class="language-buttons {}">{}</span>'.format(
            span_classes, " ".join(buttons)
        )

    def save_model(self, request, obj, form, change):
        lang = obj.get_current_language()
        if lang != "en":
            pofiles = [pofile(f) for f in find_pos(lang)]
            for po in pofiles:
                if po.percent_translated() != 100:
                    messages.add_message(
                        request,
                        messages.WARNING,
                        format_lazy(
                            _("{lang} translations for the site are not complete. "
                            "User may experience inconsistent language behavior."),
                            lang=get_language_info(lang)["name_translated"]
                        )
                    )

        return super().save_model(request, obj, form, change)


class DocumentationAdmin(SortableAdminMixin, TranslatableAdmin):
    pass

class ExtraStyleInline(admin.TabularInline):
    extra = 0
    model = ExtraStyle

class ConfigurationAdmin(SingletonModelAdmin):
    inlines = [ExtraStyleInline]

admin.site.register(Page, PageAdmin)
admin.site.register(Configuration, ConfigurationAdmin)
admin.site.register(Documentation, DocumentationAdmin)
